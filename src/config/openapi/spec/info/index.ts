const info = {
  title: 'tezos-api-gateway',
  description: 'Webservice responsible to interact with Tezos Blockchain',
  version: '0.15.0',
};

export default info;
